package api;

import com.aqa.course.api.models.Customer;
import com.aqa.course.api.models.request.Requests;
import com.aqa.course.api.models.request.SuccessfulRequests;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;
import java.util.List;

/**
 * BaseApiTest contains API tests setup and data clean up.
 *
 * @author alexpshe
 * @version 1.0
 */
public class BaseApiTest {
    protected SoftAssertions softly;
    protected Requests requests;
    protected SuccessfulRequests successfulRequests;
    protected List<Customer> createdCustomers;

    @BeforeAll
    public static void restAssured() {
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
    }

    @BeforeEach
    public void setupTest() {
        requests = new Requests();
        successfulRequests = new SuccessfulRequests();
        softly = new SoftAssertions();
        createdCustomers = new ArrayList<>();
    }

    @AfterEach
    public void cleanData() {
        createdCustomers.forEach(customer -> successfulRequests.deleteCustomer(customer.getId()));
    }
}
